# Nostr Request Body Cheet Sheets

## 0. 前提
`websocat` を使ってNostrのリレーサーバーにRequestを投げるときのJSONを例示していきます。

## 1. Metadata( kind=0 )
ユーザーのprofile情報など。display_nameとかが取れる。
### requst
```bash
echo '
[
  "REQ",
  "subscription_id_brabrabra",
  {
    "limit": 1,
    "kinds": [
      0
    ],
    "authors": [
      "a3bce095cd0e8348b094c9201bd0c3270eafaf78f776517f4101326daf5cb7ff"
    ]
  }
]' | websocat -n wss://universe.nostrich.land
```
### response
```json
[
  "EVENT",
  "subscription_id_brabrabra",
  {
    "content": "{\"lud06\":\"\",\"display_name\":\"淀川\",\"website\":\"https://crayfish-ghost.github.io/\",\"name\":\"yodogawa\",\"about\":\"ホラーや怪談関連の創作を趣味でしています\",\"picture\":\"https://avatars.githubusercontent.com/u/83725933?v=4\"}",
    "created_at": 1679278273,
    "id": "e0d7f02b2e9110d623d9e13abf603b0f80c96020235877ce28e1c2aa41d5e2c9",
    "kind": 0,
    "pubkey": "a3bce095cd0e8348b094c9201bd0c3270eafaf78f776517f4101326daf5cb7ff",
    "sig": "2b680046e39780cb551996c05d1659fb139938f1647a65e355c4dac4ab708ce31a9cb136406c1da490baa5ffc345485aafab20f2591afb4a312a5b07ca5bd9e1",
    "tags": []
  }
]
```

## 2. Short Text Note ( kind=1 )
twitterにおける"タイムライン"の取得。
`"limit":0` で永続的に取得。

### 2.1 request (authorなし)
universeタイムライン。
```bash
echo '
[
  "REQ",
  "subscription_id_brabrabra",
  {
    "limit": 0,
    "kinds": [
      1
    ]
  }
]' | websocat -n wss://universe.nostrich.land
```

### response
```json
[
  "EVENT",
  "subscription_id_brabrabra",
  {
    "sig": "b371f4a11aa8fd0a2192645039ac66cc414ca50714b468191fc639bc44a0e996f27fc96a0db697c937356a5c233f6f6bccba4f1274844554ccb1b37dfe576328",
    "kind": 1,
    "created_at": 1679308201,
    "id": "f6b2b56e193cade9cef6c101d450d39aad88e9f557d431a8d352d0e88bb67fee",
    "content": "‐‐‐‐‐‐‐‐19:30‐‐‐‐‐‐‐‐",
    "pubkey": "87e02cd9151cbf69ba20268a2a4237ad2f39fc631c96558e294ca00586477412",
    "tags": []
  }
]
```
### 2.2 request( authorあり )
ユーザータイムライン。
```bash
echo '
[
  "REQ",
  "subscription_id_brabrabra",
  {
    "limit": 0,
    "kinds": [
      1
    ],
    "authors": [
      "a3bce095cd0e8348b094c9201bd0c3270eafaf78f776517f4101326daf5cb7ff"
    ]
  }
]' | websocat -n wss://universe.nostrich.land
```
### response
```json
[
  "EVENT",
  "subscription_id_brabrabra",
  {
    "content": "さんぷる",
    "created_at": 1679308546,
    "id": "8b31168414b4a34bc4e4a3fba7ca872b079cdf4ce65ff51be1180da30f7be4e3",
    "kind": 1,
    "pubkey": "a3bce095cd0e8348b094c9201bd0c3270eafaf78f776517f4101326daf5cb7ff",
    "sig": "471c021f41135be7153fd3435479e6053263ff121df192bfacab10bf88c1ca95699ed1d8b47f652445b53416af01fb70f1ab386eed1ebc93339c4411c5a2e093",
    "tags": []
  }
]
```
## 3. ContactList( kind=3 )
いわゆるフォローリスト。
### request
```bash
echo '
[
  "REQ",
  "subscription_id_brabrabra",
  {
    "limit": 1,
    "kinds": [
      3
    ],
    "authors": [
      "a3bce095cd0e8348b094c9201bd0c3270eafaf78f776517f4101326daf5cb7ff"
    ]
  }
]' | websocat -n wss://universe.nostrich.land
```

### response
```json
[
  "EVENT",
  "subscription_id_brabrabra",
  {
    "content": "{\"wss://universe.nostrich.land?lang=ja\":{\"write\":true,\"read\":true}",
    "created_at": 1679281723,
    "id": "85dd6974360b0d2bb3a3090ef70a9d7e71ea4e5963cbb0f5b093ad65811f6ea0",
    "kind": 3,
    "pubkey": "a3bce095cd0e8348b094c9201bd0c3270eafaf78f776517f4101326daf5cb7ff",
    "sig": "20765324abcb9be6f86414682166f00830cec1d0ef6255242b340548cd5c23163274df40267ee2ee2bc3fd2a257d8518ab9c84d6a2c282a4e5a14670f777ce58",
    "tags": [
      [
        "p",
        "a3bce095cd0e8348b094c9201bd0c3270eafaf78f776517f4101326daf5cb7ff"
      ],
      [
        "p",
        "cef718fb3ab52d6242d09a3293d2f1ec24698622581105a6b9aac0b3a4e49fbc"
      ]
    ]
  }
]
```

## 4. Send Event (kind=1)
eventの送信。要は書き込み。面倒。
### request

```bash
echo '
[
  "EVENT",
  {
    "id": 59c1cf11a3e9e128c6fd5402f41e8ae0c0c7fbab570203d7410518be68c3115f
    "pubkey": "a3bce095cd0e8348b094c9201bd0c3270eafaf78f776517f4101326daf5cb7ff",
    "created_at": 1679313544,
    "kind": 1,
    "tags": [
      [
      ]
    ]
    "content": "ここにpostしたい内容を書く(UTF-8)",
    "sig": <64-bytes hex of the signature of the sha256 hash of the serialized event data, which is the same as the "id" field>
  }
]' |  websocat -n wss://relay.damus.io
```
#### idとsig
idとsigを作るために、まずは`serialized event` を作る。
```
[
  0,
  <pubkey, as a (lowercase) hex string>,
  <created_at, as a number>,
  <kind, as a number>,
  <tags, as an array of arrays of non-null strings>,
  <content, as a string>
]
```
idはこの`serialized event` をシリアライズしてsha256でhash化したもの。[参考(nostr-tools)](https://github.com/nbd-wtf/nostr-tools/blob/master/event.ts#L76)
sigはidと秘密鍵を使った署名。[参考(nostr-tools)](https://github.com/nbd-wtf/nostr-tools/blob/master/event.ts#L109)
