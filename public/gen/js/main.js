$(function(){

    let vhp = {};

    const hp = {
        'type': {
            'name' : '性別',
            'val' : ['男性','女性','その他(男女以外の性自認)'],
            'p' : [45,90,100]
        },
        'age': {
            'name': '年齢',
            'val' : ['10代','20代','30代','40代','50代','60代']
        },
        'parent' : {
            'name' : '両親',
            'val' : ['両親が健在','片親(父)','片親(母)','過去にいたが両親とも亡くなった','いた記憶がない'],
            'p' : [70,77,85,93,100]
        },
        'brosis' : {
            'name':'兄妹・姉妹',
            'val' : ['兄弟・姉妹がいる','いない','いたが亡くなった'],
            'p' : [45,90,100]
        },
        'famsip' : {
            'name' : '親族との関係',
            'val' : ['親兄弟とは仲が良い','普通','仲が悪い','絶縁状態',null],
            'p' : [30,70,90,100,200],
            'condf' : // 親兄弟がいない場合は非表示
            function(vhp){
                if(vhp.parent.i > 2 && vhp.brosis.i > 0){
                    return {
                        'i': 4,
                        'val' : null
                    };
                }
                return f.get_weight_param(hp.famsip);
            },
        },
        'mariage' : {
            'name' : '婚姻',
            'val' : ['結婚している', 'していない', '内縁関係'],
            'p' : [45,90,100],
            'condf' : //10代では結婚していない一択
            function(vhp){
                if(vhp.age.i === 0){
                    return {
                        'i': 1,
                        'val' : hp.mariage.val[1]
                    };
                }
                return f.get_weight_param(hp.mariage);
            },
        },
        'lovers' : {
            'name' : '恋人',
            'val' : ['特定のパートナーがいる','いない'],
            'condf' : //結婚,内縁関係にある場合は非表示
            function(vhp){
                if(vhp.mariage.i === 0 || vhp.mariage.i === 2){
                    return {
                        'i': 2,
                        'val' : null
                    };
                }
                return f.get_normal_param(hp.lovers);
            },
        },
        'chiled' : {
            'name' : '子供',
            'val' : ['子供がいる','いない','いたが亡くなった',null],
            'p' : [45,90,100,200],
            'condf' : //10代は非表示
            function(vhp){
                if(vhp.age.i === 0){
                    return {
                        'i': 3,
                        'val' : null
                    };
                }
                return f.get_weight_param(hp.chiled);
            }
        },
        'edb'    : {
            'name' : '学歴',
            'val' : ['大卒・院卒','高卒','中卒','義務教育を受けていない'],
            'p' : [65,80,98,100],
            'condf' : //10代は独立テーブルで判定
            function(vhp){
                let sobj = {
                    'val':['高卒','中卒','在学中','教育を受けていない'],
                    'p':[32,64,96,100]
                };
                if(vhp.age.i === 0){
                    return f.get_weight_param(sobj);
                }
                return f.get_weight_param(hp.edb);
            }
        },
        'divorce': {
            'name':'離婚歴',
            'val' : ['離婚歴あり',null],
            'p' : [10,100],
            'condf' : //10代は非表示
            function(vhp){
                if(vhp.age.i === 0){
                    return {
                        'i': 1,
                        'val' : null
                    };
                }
                return f.get_weight_param(hp.divorce);
            }
        },
        'arrest' : {
            'name' : '逮捕歴',
            'val' : ['逮捕・補導歴あり',null],
            'p' : [5,100]
        },
        'from' : {
            'name' : '出身',
            'val' : ['都会出身','田舎出身']
        },
        'job' : {
            'name' : '職業',
            'val' : ['企業経営者','会社員','公務員','個人事業主','フリーター','反社','無職・その他',null],
            'p' : [5,50,65,75,85,88,100,200],
            'condf' : //10代は非表示
            function(vhp){
                if(vhp.age.i === 0){
                    return {
                        'i': 7,
                        'val' : null
                    };
                }
                return f.get_weight_param(hp.job);
            }
        },
        'money' : {
            'name' : '家計',
            'val' : ['お金持ち','貧乏',null]
        },
        'talent' : {
            'name' : '知能',
            'val' : ['天才','秀才','おバカ',null],
            'p' : [10,30,50,100]
        },
        'height' : {
            'name' : '身長',
            'val' : ['高身長','低身長',null],
            'p' : [15,30,100]
        },
        'app' : {
            'name' : '容姿',
            'val' : ['容姿が良い','容姿が悪い',null],
            'p' : [15,30,100]
        },
        'weight' : {
            'name' : '体重',
            'val' : ['肥満','太りぎみ','痩せぎみ','痩せすぎ',null],
            'p' : [5,20,35,40,100]
        },
        'health' : {
            'name': '健康状態',
            'val' : ['不健康','身体的なハンディキャップがある','精神的なハンディキャップがある','病弱','余命数年','健康'],
            'p' : [20,25,30,40,41,100]
        },
        'tiger' : {
            'name':'その他',
            'val' : ['過去に大きなトラウマがある',null],
            'p' : [3,100]
        }
    };

    const f = {
        get_weight_param : function(obj){
            let rnum = Math.floor(Math.random() * 100);
            for( let i = 0; i < obj.p.length ; i++){
                if(rnum < obj.p[i]){
                    return {
                        'i' : i,
                        'val' : obj.val[i]
                    };
                }
            }
        },
        get_normal_param : function(obj){
            let rand_num = Math.floor(Math.random() * obj.val.length);
            return {
                'i' : rand_num,
                'val' : obj.val[ rand_num ]
            };
        },
        get_param : function(obj){
            if('condf' in obj ){
                return obj.condf(vhp);
            }else if('p' in obj){
                return f.get_weight_param(obj);
            }else{
                return f.get_normal_param(obj);
            }
        },
        get_type : function(){
            return f.get_param(hp.type);
        },
        get_age : function(){
            return f.get_param(hp.age);
        },
        get_parent : function(){
            return f.get_param(hp.parent);
        },
        get_brosis : function(){
            return f.get_param(hp.brosis);
        },
        get_famsip : function(){
            return f.get_param(hp.famsip);
        },
        get_all_params : function(){
            vhp.type = f.get_type();
            vhp.age = f.get_age();
            vhp.parent = f.get_parent();
            vhp.brosis = f.get_brosis();
            vhp.famsip = f.get_famsip();
            vhp.mariage = f.get_param(hp.mariage);
            vhp.lovers = f.get_param(hp.lovers);
            vhp.chiled = f.get_param(hp.chiled);
            vhp.edb = f.get_param(hp.edb);
            vhp.divorce = f.get_param(hp.divorce);
            vhp.arrest = f.get_param(hp.arrest);
            vhp.from = f.get_param(hp.from);
            vhp.job = f.get_param(hp.job);
            vhp.money = f.get_param(hp.money);
            vhp.talent = f.get_param(hp.talent);
            vhp.height = f.get_param(hp.height);
            vhp.app = f.get_param(hp.app);
            vhp.weight = f.get_param(hp.weight);
            vhp.health = f.get_param(hp.health);
            vhp.tiger = f.get_param(hp.tiger);
        },
        exec : function(){
            f.vhp_initialize();
            f.get_all_params();
            f.set_params_to_table();
        },
        vhp_initialize : function(){
            vhp = new Array();
        },
        set_params_to_table : function(){
            $('#hp_table > tbody').empty();

            Object.keys(vhp).forEach(function(key) {
                if(vhp[key].val === null){return;}
                let tr = $('<tr>');
                tr.append( $('<td>',{text:hp[key]['name']}) )
                    .append( $('<td>', {text:vhp[key].val}) );
                $('#hp_table > tbody').append(tr);
            });

        }
    };
    
    $('#gen-button').click(function(){
        f.exec();
    });
    
    f.exec();
});
